FROM node:latest AS base

ENV DEBIAN_FRONTEND=noninteractive

USER root
RUN apt-get update \
    # Why we have postgres library files in node images is unknown to me
    && apt-get purge -y libpq-dev libpq5 \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        vim-tiny \
        telnet \
        less \
    && apt-get clean && rm -rf /var/cache/apt/* \
    && npm install -g npm@latest

COPY ./npmrc /root/.npmrc

WORKDIR /src/app
COPY dev-bin dev-bin

FROM base AS tap
COPY package-tap.json package.json
RUN ./dev-bin/docker-npm

FROM base AS revealjs
COPY package-revealjs.json package.json
RUN ./dev-bin/docker-npm && npm link reveal-md

FROM tap AS vue
COPY package-vue.json package.json
RUN ./dev-bin/docker-npm
