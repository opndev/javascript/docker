# OPN Development Javascript docker images

Welcome!

In this repository you'll find build instructions for docker images which use
Javascript.

## Development images

The following images should be used for development of modules and applications

### Default image

registry.gitlab.com/opndev/javascript/docker:latest

This is the default layer for all JS development, it includes the [TAP testing
module](https://www.npmjs.com/package/tap).


### Vue

registry.gitlab.com/opndev/javascript/docker/vue:latest

The vue image is the latest and greatest vue image there is, currently it
targets vue3

### Vue 2

registry.gitlab.com/opndev/javascript/docker/vue2:latest

The vue2 image can be used for vue2 projects

### RevealJS

registry.gitlab.com/opndev/javascript/docker/revealjs:latest

The revealjs project builds a revealjs image for your consumption

### Base

registry.gitlab.com/opndev/javascript/docker/base:latest

This image acts as a building block for the images mentioned above.  It should
not be used for development, but rather as a building block for other images.

## Build process

You can run ./dev-bin/docker-build to locally build all the images. It is also
the script that is used for the Gitlab CI.
